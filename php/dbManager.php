<?php
/**
 *s
 *
 */
class DbManager
{
  function __construct()
  {
    $this->conn= mysqli_connect('localhost', 'root', '123789kg', 'health');
    mysqli_set_charset($this->conn, "utf8");
  }

  function customSelect($sql){
      $res= array();
      $index=0;
      if($result=mysqli_query($this->conn, $sql)){
          if(mysqli_num_rows($result)>0){
              while($row=mysqli_fetch_assoc($result)){
                  $res[$index]=$row;
                  $index++;
              }
          }
      }
      return $res;
  }

  function customSql($sql){
    if(mysqli_query($this->conn, $sql)){
      return 'ok';
    }else{
      return mysqli_error($this->conn);
    }

  }

  function select($table, $columns="*", $statement="none"){
    $sql="SELECT ";
    if($columns!="*"){
      for($i=0; $i<count($columns); $i++){
        if($i!=(count($columns)-1)){

          $sql.=mysqli_real_escape_string($this->conn,$columns[$i]).", ";
        }else{
          $sql.=mysqli_real_escape_string($this->conn,$columns[$i])." ";
        }
      }
    }else{
      $sql.=$columns." ";
    }

    $sql.="FROM ".mysqli_real_escape_string($this->conn,$table);

    if($statement!="none"){
      $sql.=" WHERE ";
      foreach($statement as $key => $value) {
        $sql.=mysqli_real_escape_string($this->conn,$key)."="."'".mysqli_real_escape_string($this->conn,$value)."' AND ";
      }

      $sql=substr($sql, 0, -5);

    }
    $res= array();
    $index=0;
    if($result=mysqli_query($this->conn, $sql)){
      if(mysqli_num_rows($result)>0){
        while($row=mysqli_fetch_assoc($result)){
          foreach($row as &$el){
            if(strpos($el, '___') !== false){
              $el = explode('___', $el);
            }
          }
          $res[$index]=$row;
          $index++;
        }
      }
    }

    return $res;

  }

  function insert($table, $columns){
    $sql="INSERT INTO ".mysqli_real_escape_string($this->conn,$table).  " (";
    foreach($columns as $key => $value) {
      $sql.=mysqli_real_escape_string($this->conn,$key).", ";
    }

    $sql=substr($sql, 0, -2);

    $sql.=") VALUES (";
    foreach($columns as $key => $value) {
      $sql.="'".mysqli_real_escape_string($this->conn,$value)."', ";
    }

    $sql=substr($sql, 0, -2);

    $sql.=")";

    if(mysqli_query($this->conn, $sql)){
      return 'ok';
    }else{
      return mysqli_error($this->conn);
    }

  }


  function update($table, $columns, $statements="none"){
    $sql="UPDATE ".mysqli_real_escape_string($this->conn,$table). " SET ";
    foreach($columns as $key => $value) {
      $sql.=mysqli_real_escape_string($this->conn,$key)."="."'".mysqli_real_escape_string($this->conn,$value)."', ";
    }

    $sql=substr($sql, 0, -2);


    if($statements!="none"){
      $sql.=" WHERE ";
      foreach($statements as $key => $value) {
        $sql.=mysqli_real_escape_string($this->conn,$key)."="."'".mysqli_real_escape_string($this->conn,$value)."' AND ";
      }

      $sql=substr($sql, 0, -5);
    }

    if(mysqli_query($this->conn, $sql)){
      return 'ok';
    }else{
      return mysqli_error($this->conn);
    }



  }


  function delete($table, $statements="none"){

    $sql="DELETE FROM ".mysqli_real_escape_string($this->conn,$table);

    if($statements!="none"){
      $sql.=" WHERE ";
      foreach($statements as $key => $value) {
        $sql.=mysqli_real_escape_string($this->conn,$key)."="."'".mysqli_real_escape_string($this->conn,$value)."' AND ";
      }

      $sql=substr($sql, 0, -5);
    }

    if(mysqli_query($this->conn, $sql)){
      return 'ok';
    }else{
      return mysqli_error($this->conn);
    }
  }


  public function createTable($name, $fields = []){

    $query = "CREATE TABLE `".$name."` ( `id` INT(11) NOT NULL AUTO_INCREMENT, `create_date` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP, ";
 
     $i = 1;
 
    foreach ($fields as $key => $field) {
      switch ($field['type']) {
        case 'text':
          $query.= "`prop".$i."` VARCHAR(500) NULL DEFAULT NULL, ";
          break;
        case 'number':
          $query.= "`prop".$i."` INT(11) NULL DEFAULT NULL, ";
          break;
        case 'checkbox':
          $query.= "`prop".$i."` TINYINT(1) NOT NULL DEFAULT 0, ";
          break;
        case 'textarea':
          $query.= "`prop".$i."` LONGTEXT NOT NULL, ";
          break;
        case 'date':
          $query.= "`prop".$i."` DATE NULL DEFAULT NULL, ";
          break;
        case 'select':
          $query.= "`prop".$i."` VARCHAR(500) NULL DEFAULT NULL, ";
          break;
      }
      $i++;
    }

    $query .= " PRIMARY KEY (`id`)) ENGINE = InnoDB";

    if(mysqli_query($this->conn, $query)){
      return true;
    }else{
      return mysqli_error($this->conn);
    }
  }

  public function addColumn($table_name, $column, $el_count, $name = 'undefined'){
    // "ALTER TABLE `test` ADD `test1` VARCHAR(500) NULL DEFAULT NULL;"
    if($name == 'undefined'){
      $query = "ALTER TABLE `".$table_name."` ADD `prop".$el_count."` ";
    }else{
      $query = "ALTER TABLE `".$table_name."` ADD `".$name."` ";
    }

    switch ($column['type']) {
        case 'text':
          $query.= " VARCHAR(500) NULL DEFAULT NULL";
          break;
        case 'number':
          $query.= " INT(11) NULL DEFAULT NULL";
          break;
        case 'checkbox':
          $query.= " TINYINT(1) NOT NULL DEFAULT 0";
          break;
        case 'textarea':
          $query.= " LONGTEXT NOT NULL";
          break;
        case 'date':
          $query.= " DATE NULL DEFAULT NULL";
          break;
        case 'select':
          $query.= " VARCHAR(500) NULL DEFAULT NULL";
          break;
      }


    if(mysqli_query($this->conn, $query)){
      return 'ok';
    }else{
      return mysqli_error($this->conn);
    }
  }
  
  public function dtSearch($columns, $table_name,  $search = "", $start, $length, $order, $dir, $filters, $all = false){
    

    $object = QueryManager::$QM -> table($table_name);

    $object = $object -> select();

    $object = $object -> where(function($q) use ($columns, $search){
      foreach($columns as $column){
        $q -> orWhere($column, 'like', '%'.$search.'%');
      }
    });
    
    foreach($filters as $column => $values){
      $object = $object -> where(function($q) use ($column, $values){
        foreach($values as $value){
          $q -> orWhere($column, '=', $value);
        }
      });
    }

    $object = $object -> orderBy($order, $dir);
    if(!$all){
      $object = $object -> limit($length) -> offset($start);
    }
    


    return $object -> get();

  }


  function __destruct(){
    mysqli_close($this->conn);
  }



}
?>
