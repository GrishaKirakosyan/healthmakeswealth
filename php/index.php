<?php

require 'dbManager.php';


$dbManager = new DbManager();

$avgTmp = 0;
$symptoms = empty($_GET['symptoms']) ? [] : $_GET['symptoms'];
foreach($symptoms as $symptom){
    $dbSymptom = $dbManager->select('symptoms', '*', ['id' => $symptom])[0];
    $avgTmp+=$dbSymptom['factor'];
}

if(count($symptoms) > 0){
    $avgTmp /= count($symptoms);
}



$months = [
    'Jan',
    'Feb',
    'Mar',
    'Apr',
    'May',
    'Jun',
    'Jul',
    'Aug',
    'Sep',
    'Oct',
    'Nov',
    'Dect'
];

$month = empty($_GET['month']) ? 'Jan' : $_GET['month'];
$year = empty($_GET['year']) ? '2019' : $_GET['year'];

$data = $dbManager->select('weather', ['Country', $month], ['year' => $year]);
$result = [];
$result['label'] = ['Country', 'Comfort','TMP'];
$dif = [];
foreach($data as $el){
    $result[$el['Country']][0] = $el['Country'];
    $dif[] = abs($avg - (float)$el[$month]);
    $result[$el['Country']][2] = (float)$el[$month];
}

$max = max($dif);
$percentage = [];
foreach($dif as $d){
    $percentage[] = $d*100/$max;
}




$result = array_values($result);
for($i = 1; $i < count($result); $i++){
    $result[$i][1] = $percentage[$i-1];
    $result[$i] = array_values($result[$i]);
    $tmp = $result[$i][1];
    $result[$i][1] = $result[$i][2];
    $result[$i][2] = $tmp;
}
$result[] = ['', 0, 0];
echo json_encode($result);
exit;
